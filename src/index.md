---
# Feel free to add content and custom Front Matter to this file.

layout: default
---

# Chess Tredeog

Chess Tredeog est un logiciel d'arbitrage qui se veut moderne et simple d'utilisation. 

Décomposé en deux parties, ils vous permettra de saisir vos résultats depuis plusieurs plateforme ou même d'organiser des tournois directement sur internet. La partie serveur comprend toute la mécanique de gestion d'arbitrage, tandis que la partie client vous permettra une saisie depuis un navigateur web, une tablette ou tout autre support ayant un navigateur internet.

----

**Chess Tredeog v0.0.1**
{:style="text-align:center"}
